# AutoHotkey Scripts | Keep ANY key held down

A simple script with AutoHotkey is keeping a key held down. I think I've come up with an interesting take on this problem that works for any key on the keyboard, but first I'm going to show you how most people solve this problem with just one key.

You can accomplish this for one key with just two lines. A `F1` Hotkey trigger, and a `Send {a down}` command. A keystroke is actually made up of two components: down and up. When the down event is triggered, the computer says "Ok its down", and only when you release the key does it say "Ok I'm released." With this script, when I tap `F1` it sends only the down portion of the `a` key, tricking the computer into thinking you are continuously holding down that key. It's kinda like half of an A press. The way you stop the key from being held is tapping the `a` key, triggering the up event. Let's go ahead and run the script.

A great way to demonstrate this is with *Minecraft*, a game I've feature before on this channel. You can see when I press `F1` I keep moving left, indicating that `a` is held down. Then I hit `a` again to stop the movement.

# Part 2

That's great and all, but with this method you would have to create a different hotkey for each key on the keyboard to accomplish my goal. My idea was to have a trigger key where by pressing it causes the next key to be pressed.

> Show GetKeyState docs
> https://www.autohotkey.com/docs/commands/GetKeyState.htm#function

With the GetKeyState() function we can set a group of hotkeys to be activated only when another key is pressed. Let's set that up in a new script.

`#if GetKeyState("F1", "P")`

Our first line creates a group of context sensitive hotkeys. These will only be active when my `F1` key is held down. In fact, using `#If` you can do all kinds of expressions to make hotkeys only active at certain times. But today this will do. 

Next we create the down commands from earlier, with a dollar sign modifier. 

`$a::Send {a down}`

> Show Hotkey docs
> https://www.autohotkey.com/docs/Hotkeys.htm

This yet another bit of autohotkey magic. Without it, after you release the `a` key it will send the up event. Which, well, ruins the whole point of this. 

Let's test it out.

Back in *minecraft* I press `a` and it doesn't keep it held down. Great. Now I hold down `F1` and then tap `a`.... and I keep moving! Great again. Now I'm tapping `a` to send the up event and end my movement. Looks like it works!

# Timelapse

I'll let you watch my VSCode magic as I set up this hotkey for every key on the keyboard. If you want this script, you can download it from my gitlab repository in the description. If you liked this video and want to see more automation scripts and tutorials, check out my other videos. See ya.